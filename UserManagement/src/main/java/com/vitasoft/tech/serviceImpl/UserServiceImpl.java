package com.vitasoft.tech.serviceImpl;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vitasoft.tech.entity.City;
import com.vitasoft.tech.entity.Country;
import com.vitasoft.tech.entity.State;
import com.vitasoft.tech.entity.User;
import com.vitasoft.tech.repository.CityRepository;
import com.vitasoft.tech.repository.CountryRepository;
import com.vitasoft.tech.repository.StateRepository;
import com.vitasoft.tech.repository.UserRepository;
import com.vitasoft.tech.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	CountryRepository countryRepo;

	@Autowired
	StateRepository stateRepo;

	@Autowired
	CityRepository cityRepo;

	@Autowired
	UserRepository userRepo;

	@Autowired
	EmailServiceImpl emailService;

	@Override
	public Map<Long, String> findCountries() {
		List<Country> countriesList = countryRepo.findAll();
		Map<Long, String> countriesMap = new HashMap<>();
		countriesList.forEach(country -> {
			countriesMap.put(country.getCountryId(), country.getCountryName());
		});
		return countriesMap;
	}

	@Override
	public Map<Long, String> findStates(Long countryId) {
		Map<Long, String> stateMap = new HashMap<>();
		List<State> statesList = stateRepo.findByCountryId(countryId);
		statesList.forEach(state -> {
			stateMap.put(state.getStateId(), state.getStateName());
		});

		return stateMap;
	}

	@Override
	public Map<Long, String> findCities(Long stateId) {
		Map<Long, String> citiesMap = new HashMap<>();
		List<City> citiesList = cityRepo.findByStateId(stateId);
		citiesList.forEach(city -> {
			System.out.println("========>" + city.getCityName());
			citiesMap.put(city.getCityId(), city.getCityName());
		});

		return citiesMap;
	}

	@Override
	public Boolean isEmailUnique(String emailId) {
		User userDetails = userRepo.findByUserEmailId(emailId);
		if (userDetails == null) {
			return true;
		}
		return false;
	}

	@Override
	public Boolean saveUser(User user) {

		user.setPassword(passwordGenerator());
		user.setAccountStatus("LOCKED");
		User userObj = userRepo.save(user);

		String emailBody = unlockAccEmailBody(user);
		String subject = "UNLOCK your account VST";
		boolean isSent = emailService.sendAccountUnlockEmail(user.getUserEmailId(), subject, emailBody);// to,subj,message

		return userObj.getUserId() != null && isSent;
	}

	public String passwordGenerator() {
		char[] password = new char[5];
		String alphaNumeric = "ABCDEFGHIabcdefghi1234567890";
		Random randomPsw = new Random();
		for (int i = 0; i < 5; i++) {
			password[i] = alphaNumeric.charAt(randomPsw.nextInt(alphaNumeric.length()));
		}
		System.out.println("password=====>" + password.toString());
		return password.toString();

	}

	@Override
	public String loginCheck(String email, String password) {
		User userDetails = userRepo.findByUserEmailIdAndPassword(email, password);
		if (userDetails != null) {
			if (userDetails.getAccountStatus().equals("LOCKED"))
				return "ACCOUNT_LOCKED";
		} else {
			return "LOGIN_SUCCESS";
		}
		return "INVALID_CREDENTIALS";
	}

	@Override
	public Boolean isTempPswValid(String email, String tempPsw) {
		User userDetails = userRepo.findByUserEmailIdAndPassword(email, tempPsw);
		return userDetails.getUserId() != null;
	}

	@Override
	public Boolean unlockAccount(String email, String newPsw) {
		User userDetails = userRepo.findByUserEmailId(email);

		userDetails.setAccountStatus("UNLOCKED");
		userDetails.setPassword(newPsw);
		try {
			userRepo.save(userDetails);
			return true;

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

	}

	@Override
	public boolean forgotPassword(String emailId) {
		User userDetails = userRepo.findByUserEmailId(emailId);
		if (userDetails != null) {

			String emailBody = passwordResend(userDetails);
			String subject = "forgot password VST";
			return  emailService.sendAccountUnlockEmail(emailId, subject, emailBody);// to,subj,message
		}else 
			return false;
			
	}

	private String passwordResend(User user) {

		String fileName = "forgot-password-email-body.txt";
		List<String> replacedLine = null;
		String mailBody = null;
		try {
			Path path = Paths.get(fileName, "");
			Stream<String> lines = Files.lines(path);
			replacedLine = lines.map(
					line -> line.replace("{FNAME}", user.getUserFirstName())
					            .replace("{LNAME}", user.getUserLastName())
							    .replace("{PWD}", user.getPassword())
							    .replace("{EMAIL}", user.getUserEmailId()))
					            .collect(Collectors.toList());
			mailBody = String.join("", replacedLine);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return mailBody;

	}

	// method called within the class in user registration

	private String unlockAccEmailBody(User user) {
//		StringBuffer sb = new StringBuffer("");
//		String newString = null;
		String fileName = "unlock-acc-email-body.txt";
		List<String> replacedLine = null;
		String mailBody = null;
		try {
			Path path = Paths.get(fileName, "");
			Stream<String> lines = Files.lines(path);
			replacedLine = lines.map(
					line -> line.replace("{FNAME}", user.getUserFirstName()).replace("{LNAME}", user.getUserLastName())
							.replace("{TEMP-PWD}", user.getPassword()).replace("EMAIL", user.getUserEmailId()))
					.collect(Collectors.toList());
			mailBody = String.join("", replacedLine);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return mailBody;
	}

}
