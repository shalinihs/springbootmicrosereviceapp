package com.vitasoft.tech.serviceImpl;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import com.vitasoft.tech.service.IEmailService;


@Service
public class EmailServiceImpl implements IEmailService {

	@Autowired
	JavaMailSender mailService;

	@Override
	public boolean sendAccountUnlockEmail(String to, String Subject, String message) {

		try {
			MimeMessage mimeMessage = mailService.createMimeMessage();

			MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage);
			
			mimeMessageHelper.setTo(to);
			mimeMessageHelper.setText(message,true);//Consider HTML message is true
			mimeMessageHelper.setSubject(Subject);
			
			
			mailService.send(mimeMessage);
			
			return true;
			
			
		} catch (MessagingException e) {
			
			e.printStackTrace();
		}
		return false;
	}

}
