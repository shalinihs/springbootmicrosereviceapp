package com.vitasoft.tech.restcontroller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.vitasoft.tech.model.LoginForm;
import com.vitasoft.tech.serviceImpl.UserServiceImpl;

@RestController
public class LoginRestController {

	@Autowired
	private UserServiceImpl userService;

	
	@PostMapping(value="/login")
	public String userLogin(@RequestBody LoginForm loginForm) {

		return userService.loginCheck(loginForm.getEmailId(), loginForm.getPassword());

	}

}
