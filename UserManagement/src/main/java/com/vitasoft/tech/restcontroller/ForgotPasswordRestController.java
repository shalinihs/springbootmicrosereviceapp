package com.vitasoft.tech.restcontroller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.vitasoft.tech.serviceImpl.UserServiceImpl;

@RestController
public class ForgotPasswordRestController {
	
	
	@Autowired
	private UserServiceImpl userService;
	
	
	@GetMapping(value="/forgotPassword")
	public String getUserPassword(@RequestParam String emailId) {
		if(userService.forgotPassword(emailId)) {
			
			return "Please Check Your EmailId for Password";
		}
		return "Please Enter Valid EmailId";
	}

}
