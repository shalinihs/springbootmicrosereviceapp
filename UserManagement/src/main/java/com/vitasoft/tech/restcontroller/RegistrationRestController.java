package com.vitasoft.tech.restcontroller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.vitasoft.tech.entity.User;
import com.vitasoft.tech.serviceImpl.UserServiceImpl;

@RestController
public class RegistrationRestController {
	@Autowired
	UserServiceImpl userService;
	
	
	
	@GetMapping(value="/loadCountries")
	public Map<Long,String> findCountries(){
		
		return userService.findCountries();
		
		
	}
	
	@GetMapping(value="/loadStates/{countryId}")
	public Map<Long,String> findStates(@PathVariable Long countryId){
		return userService.findStates(countryId);
	}
	
	@GetMapping(value="/loadCities/{stateId}")
	public Map<Long,String> findCities(@PathVariable Long stateId){
		return userService.findCities(stateId);
	}
	
	@GetMapping(value="/emailcheck/{emailId}")
	public String isEmailIdUnique(@PathVariable String emailId) {
		if(userService.isEmailUnique(emailId)) {
			return "EmailId Unique";
			
		}
		return "EmailId not Unique";
	}
	
	@PostMapping(value="/registration",consumes=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> userRegister(@RequestBody User user) {
		if(userService.isEmailUnique(user.getUserEmailId())) {
			 userService.saveUser(user);
			return new ResponseEntity<String>( "Registration Success",HttpStatus.CREATED);
		}
		return new ResponseEntity<String>("Registration Failed",HttpStatus.BAD_REQUEST);
		
		
	}
	

	

}
