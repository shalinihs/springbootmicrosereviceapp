package com.vitasoft.tech.restcontroller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.vitasoft.tech.model.UnlockAccount;
import com.vitasoft.tech.serviceImpl.UserServiceImpl;


@RestController
public class UnlockAccountRestController {
	
	@Autowired
	private UserServiceImpl userService;
	
	@PostMapping(value="/unlockUserAccount")
	public String unlockUserAccount(@RequestBody UnlockAccount unlockAcc) {
		if(userService.isTempPswValid(unlockAcc.getEmail(), unlockAcc.getTempPsw())) {
			userService.unlockAccount(unlockAcc.getEmail(), unlockAcc.getNewPsw());
			return "Account unlocked , Please proceed with ligin";
		}
		return "Please Enter valid temporary password";
		
	}
	
	
	
	

}
