package com.vitasoft.tech.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.google.common.base.Predicate;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

	@Bean
	public Docket postsApi() {
		return new Docket(DocumentationType.SWAGGER_2)
				.groupName("public-api")
				.apiInfo(apiInfo())
				.select()
				.apis(RequestHandlerSelectors.basePackage("com.vitasoft.tech.restcontroller"))
				.paths(PathSelectors.any()) //documentation for all path
				.build();
	}


	
	private ApiInfo apiInfo() {
		return new ApiInfoBuilder().title("User Management API")
				.description("User Management API reference for developers")
				.termsOfServiceUrl("http://vitasoft-tech.com")
				.contact("shalini@gmail.com").license("VitasoftTech License")
				.licenseUrl("shalini@gmail.com").version("1.0").build();
	}
	
	
}