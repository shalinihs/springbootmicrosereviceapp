package com.vitasoft.tech.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vitasoft.tech.entity.Country;

public interface CountryRepository extends JpaRepository<Country,Long> {

}
