package com.vitasoft.tech.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vitasoft.tech.entity.State;

public interface StateRepository extends JpaRepository<State,Long>{
	//Here CountryId is not primaryKey Custom ID 
	
	 List<State> findByCountryId(Long countryId);

}
