package com.vitasoft.tech.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.vitasoft.tech.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User,Long>{
	
	
	//Email is not primary key 
	 User findByUserEmailId(String emailId);
	
	 User findByUserEmailIdAndPassword(String emailId,String password);
	

}
