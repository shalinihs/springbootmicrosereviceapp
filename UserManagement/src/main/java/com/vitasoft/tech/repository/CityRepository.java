package com.vitasoft.tech.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vitasoft.tech.entity.City;

public interface CityRepository extends JpaRepository<City,Long> {
	
	//state Id is not primary key
	 List<City> findByStateId(Long stateId);

}
