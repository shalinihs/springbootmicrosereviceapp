package com.vitasoft.tech.model;

import lombok.Data;

@Data
public class LoginForm {
	private String emailId;
	private String password;

}
