package com.vitasoft.tech.model;

import lombok.Data;

@Data
public class UnlockAccount {
	private String email;
	private String tempPsw;
	private String newPsw;
	private String confirmPsw;

}
