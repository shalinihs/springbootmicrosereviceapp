package com.vitasoft.tech.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

@Data
@Entity
public class User {
	@Id
	@Column(name="userId")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long userId;
	
	@Column(name="first_name")
	private String userFirstName;
	
	@Column(name="last_name")
	private String userLastName;
	
	@Column(name="Email")
	private String userEmailId;
	
	@Column(name="phone_number")
	private String phoneNumber;
	
	@Column(name="date_of_birth")
	private Date dateOfBirth;
	
	@Column(name="gender")
	private String gender;
	
	@Column(name="city_Id")
	private Long cityId;
	
	@Column(name="state_Id")
	private Long stateId;
	
	@Column(name="country_Id")
	private Long countryId;
	
	@Column(name="password")
	private String password;
	
	@Column(name="account_status")
	private String accountStatus;
	

}
