package com.vitasoft.tech.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name="countries_master")
public class Country {
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long countryId;
	
	@Column (name="shortname")
	private String countryShortName;
	
	@Column(name="name")
	private String countryName;
	

}
