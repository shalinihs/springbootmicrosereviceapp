package com.vitasoft.tech.service;

import com.vitasoft.tech.entity.User;

public interface IEmailService {
	
	public boolean sendAccountUnlockEmail(String to,String Subject,String message) ;

}
