package com.vitasoft.tech.service;

import java.util.Map;

import com.vitasoft.tech.entity.User;

public interface UserService {
	
	//User Registration Page Operation
	
	public Map<Long, String> findCountries();
	
	public Map<Long,String> findStates(Long countryId);
	
	public Map<Long,String> findCities(Long stateId);
	
	public Boolean isEmailUnique(String emailId);
	
	public Boolean saveUser(User user);
	
	//Login Page Operation
	
	public String loginCheck(String email,String password);
	
	//Unlock Account operation 
	
	public Boolean isTempPswValid(String email,String tempPsw);
	
	public Boolean unlockAccount(String email,String newPsw);
	
	//forgot password operation
	
	public boolean forgotPassword(String emailId);
	
	
	


}
