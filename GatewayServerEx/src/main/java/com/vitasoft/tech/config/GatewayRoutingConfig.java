package com.vitasoft.tech.config;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GatewayRoutingConfig {
	@Bean
	public RouteLocator configureRoute(RouteLocatorBuilder builder) {
		System.out.println("GatewayRoutingConfig.........");
		return builder.routes()
				.route(
						"employeeServiceId", 
						r->r.path("/employee/**")//changed from /api/employee/**
						.uri("lb://EMPLOYEE-SERVICE")
						)
				/*.route(
						"productserviceid", 
						r->r.path("/product/**")
						.uri("lb://PRODUCT-SERVICE")
						)*/
				.build();
	}
}