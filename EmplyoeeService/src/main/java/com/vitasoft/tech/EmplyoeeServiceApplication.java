package com.vitasoft.tech;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient //optional in 5.2... higher spring boot version 

public class EmplyoeeServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmplyoeeServiceApplication.class, args);
	}

}
