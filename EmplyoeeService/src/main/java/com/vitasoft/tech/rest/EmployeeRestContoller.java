package com.vitasoft.tech.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vitasoft.tech.entity.Employee;
import com.vitasoft.tech.exception.EmployeeNotFoundException;
import com.vitasoft.tech.service.IEmployeeService;

@RestController
@RequestMapping(value="/employee") //Multi level path is not allowed
                                   //so "/api/employee" changed to "/employee"
                                  //upto spring boot Application it works but for Gate way It will not work
//@CrossOrigin("http://localhost:4200")
public class EmployeeRestContoller {
	
	@Autowired
	IEmployeeService service;//Has a relation
	
	@PostMapping("/create")
	public ResponseEntity<String> createEmployee(
			                      @RequestBody Employee employee){
		
		Long id=service.createEmployee(employee);
		String message="Employee + '"+id+"' Created";
		return new ResponseEntity<String>(message,HttpStatus.OK);
		
		
	}
	
	@GetMapping("/all")
	public ResponseEntity<List<Employee>> findAllEmployee(){
		
		List<Employee> list=service.findAllEmployee();
		return new ResponseEntity<List<Employee>>(list,HttpStatus.OK);
	}
	
	@GetMapping("/find/{id}")
	public ResponseEntity<?> findOneEmployee(@PathVariable Long id) {
		ResponseEntity<?> resp=null;
		try {
		Employee emp=service.findOneEmployee(id);
		resp=new ResponseEntity<Employee>(emp,HttpStatus.OK);
		
		}catch(EmployeeNotFoundException e) {
			e.printStackTrace();
			throw e;
			
		}
		
		//Common Code in remove find api
//		catch(EmployeeNotFoundException e) {
//			e.printStackTrace();
//			resp=new ResponseEntity<String>(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);//500
//		}
		
		
		
		
		return resp  ;
	
		
	}
	
	@DeleteMapping("/remove/{id}")
	public ResponseEntity<String> deleteOneEmployee(@PathVariable Long id){
		ResponseEntity<String> resp=null;
		try {
			service.deleteOneEmployee(id);
			resp=new ResponseEntity<String>("Employee "+id+" Deleted",HttpStatus.OK);
			
		}catch(EmployeeNotFoundException e) {
			e.printStackTrace();
			throw e;
		}
		
		
		//Common Code in remove and find api
//		catch(EmployeeNotFoundException e) {
//			e.printStackTrace();
//			resp=new ResponseEntity<String>(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
//			
//		}
		
		return resp;
			
	}
	
	@PutMapping("/modify")
	public ResponseEntity<String> updateEmployee(@RequestBody Employee employee){
		
		ResponseEntity<String> resp=null;
		try {
		service.updateEmployee(employee);
		resp=new ResponseEntity<String>("Employee Updated",HttpStatus.OK);
		}catch(EmployeeNotFoundException e) {
			e.printStackTrace();
			throw e;//call global handler
		}
		
		return resp;
	}
	
	@PatchMapping("/modify/name/{eid}/{ename}")
	public ResponseEntity<String> updateEmployeeName(@PathVariable String ename,@PathVariable Long eid){
		ResponseEntity<String> resp=null;
		try {
			service.updateEmployeeName(ename, eid);
			
			resp=new ResponseEntity<String>("Employee name Updated",HttpStatus.OK);
		}
		catch(EmployeeNotFoundException e) {
			e.printStackTrace();
			throw e; // Call Golbal Handler
		}
		return resp;
	}
	
	

}
